#![feature(pattern_parentheses)]
#![feature(duration_as_u128)]

extern crate specs;

use specs::prelude::*;

extern crate nalgebra as na;
extern crate ncollide2d;
extern crate nphysics2d;
extern crate shred;

#[macro_use]
extern crate shred_derive;

#[macro_use]
extern crate log;
extern crate log4rs;

extern crate rustyline;

#[cfg(feature = "graphical")]
extern crate nphysics_testbed2d;

use rustyline::error::ReadlineError;
use rustyline::Editor;

use na::{Isometry2, Point2, Vector2};
use ncollide2d::shape::{Cuboid, ShapeHandle};
use nphysics2d::object::{BodyHandle, Material};
use nphysics2d::volumetric::Volumetric;
use std::sync::mpsc;
use std::sync::Arc;
use std::sync::RwLock;
use std::cell::RefCell;

const COLLIDER_MARGIN: f32 = 0.01;

#[derive(Clone, Debug)]
struct PhysicsBody(BodyHandle);

impl Component for PhysicsBody {
    // Storage is used to store all data for components of this type
    // VecStorage is meant to be used for components that are in almost every entity
    type Storage = VecStorage<Self>;
}

// Resources
struct PhysicsWorld {
    #[cfg(feature="lightweight")]
    physics_world: Box<nphysics2d::world::World<f32>>,
    #[cfg(feature="graphical")]
    physics_world: Box<nphysics_testbed2d::WorldOwner + Send + Sync>,
}

    use ::std::ops::Deref;
    use ::std::ops::DerefMut;
impl PhysicsWorld {
    #[cfg(feature="lightweight")]
    pub fn new(physics_world : Box<nphysics2d::world::World<f32>>) -> Self {
        return PhysicsWorld{physics_world}
    }
    #[cfg(feature="graphical")]
    pub fn new(physics_world : Box<nphysics_testbed2d::WorldOwner + Send + Sync>) -> Self {
        return PhysicsWorld{physics_world}
    }
    fn get<'a: 'b, 'b>(&'a self) -> Box<Deref<Target=nphysics2d::world::World<f32>> + 'b> {
        #[cfg(feature="lightweight")]
        return self.physics_world;
        #[cfg(feature="graphical")]
        return self.physics_world.get();
    }
    fn get_mut<'a: 'b, 'b>(&'a mut self) -> Box<DerefMut<Target=nphysics2d::world::World<f32>> + 'b> {
        #[cfg(feature="lightweight")]
        return self.physics_world;
        #[cfg(feature="graphical")]
        return self.physics_world.get_mut();
    }
}

impl Default for PhysicsWorld {
    fn default() -> Self {
        PhysicsWorld { physics_world: Box::new(nphysics2d::world::World::new())}
    }
}

// SystemData
struct PhysicsSystem;

impl PhysicsSystem {
    pub fn new() -> Self {
        PhysicsSystem {}
    }
}

impl<'a> System<'a> for PhysicsSystem
     {
    // These are the resources required for execution.
    // You can also define a struct and `#[derive(SystemData)]`,
    // see the `full` example.
    type SystemData = (
        Write<'a, PhysicsWorld>,
    );

    fn run(&mut self, data: Self::SystemData) {
        #[cfg(feature="lightweight")]
        {
            let (world) = data;

            // `Read` implements `Deref`, so it
            // coerces to `&DeltaTime`.
            let mut world = world.0;

            world.get_mut().step();
        }
    }
}

// SystemData
struct PrinterSystem;

impl<'a> System<'a> for PrinterSystem
     {
    // These are the resources required for execution.
    // You can also define a struct and `#[derive(SystemData)]`,
    // see the `full` example.
    type SystemData = (
        Read<'a, PhysicsWorld>,
        ReadStorage<'a, PhysicsBody>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let world = data.0;

        for (handle) in (&data.1).join() {
            info!("{:#?}", world.get().rigid_body(handle.0).expect("no rigid body").position());
        }
    }
}

// TODO: input system for admin to issue commands, Useful for manual testing and surprises/debgging/support.
// SystemData
struct InputSystem;

impl<'a> System<'a> for InputSystem
     {
    // These are the resources required for execution.
    // You can also define a struct and `#[derive(SystemData)]`,
    // see the `full` example.
    type SystemData = (
    );

    fn run(&mut self, data: Self::SystemData) {
        
    }
}


mod command {
    use ::*;

    #[derive(Debug, Clone)]
    pub enum Definition {
        Log(()),
        /// TODO: add different types of creation
        Create(()),
        Quit(()),
    }
}

mod input {
    use ::*;
    use rustyline::completion::Completer;


    struct Hinter<'a> {
        commands: &'a Vec<String>,
    }
    impl<'a> Hinter<'a> {
        pub fn new(commands: &'a Vec<String>) -> Hinter<'a> {
            Hinter {
                commands,
            }
        }
    }

    impl<'a> Completer for Hinter<'a> {
        fn complete(&self, line: &str, pos: usize) -> rustyline::Result<(usize, Vec<String>)> {
            let commands : &'a Vec<String> = self.commands;
            Ok((0, commands.into_iter().cloned()
                .filter(|word| word.starts_with(line))
                .collect()))
        }
    }

    pub struct Loop {
        tx: mpsc::Sender<command::Definition>,
    }

    impl Loop {
        pub fn new(tx: mpsc::Sender<command::Definition>) -> Loop {
            Loop {
                tx
            }
        }

        /// Designed to run it it's own thread.
        /// Send messages when a command is received.
        /// Anyone can catch the mesage to react to it. That's the role of InputSystem.
        pub fn launch(&self) {
            // Both words and commands must have same size.
            let words = vec![
                "logs".to_string(),
                "create".to_string(),
                "quit".to_string(),
            ];
            let commands = vec![
                command::Definition::Log(()),
                command::Definition::Create(()),
                command::Definition::Quit(()),
            ];
            let mut rl = Editor::<Hinter>::new();
            let completer = Hinter::new(&words);
            rl.set_completer(Some(completer));
            if rl.load_history("history.txt").is_err() {
                debug!("No previous history.");
            }
            loop {
                let readline = rl.readline(">> ");
                match readline {
                    Ok(line) => {
                        rl.add_history_entry(line.as_ref());
                        let index: usize = match words
                            .iter()
                            .enumerate()
                            .find(|&r| r.1 == &line) 
                            {
                                Some(found) => found.0,
                                _ => {
                                    debug!("Unknown command: {:?}", line);
                                    continue;
                                },
                            };
                        let command = commands[index].clone();
                        self.tx.send(command.clone()).unwrap();
                        debug!("Command sent: {:?}", command);
                    },
                    Err(ReadlineError::Interrupted) => {
                        debug!("CTRL-C");
                        break
                    },
                    Err(ReadlineError::Eof) => {
                        debug!("CTRL-D");
                        break
                    },
                    Err(err) => {
                        debug!("Error: {:?}", err);
                        break
                    }
                }
            }
        }
        fn logMode() {

        }
    }
}

mod event {
    use ::*;
    pub struct Listener {
        rx: mpsc::Receiver<command::Definition>
    }

    impl Listener {
        pub fn new(rx: mpsc::Receiver<command::Definition>) -> Listener {
            Listener { rx }
        }
        pub fn poll(&self) -> Option<command::Definition> {
            match self.rx.try_recv() {
                Ok(command) => Some(command),
                Err(_) => {
                    None
                }
            }
        }
    }
}

mod game {
    use ::*;
    use std::rc::Rc;

    pub trait Loop {
        fn start<F>(self, listener: event::Listener, loop_function: F)
            where F: Fn(Rc<&mut Self>);
    }

    #[cfg(feature = "lightweight")]
    pub mod core {

        use ::*;
        use std::rc::Rc;
        pub struct CoreLoop<'a, 'b> {
            dispatcher: specs::Dispatcher<'a, 'b>,
            world: specs::World
        }

        impl<'a, 'b> CoreLoop<'a, 'b> {
            pub fn new(
                dispatcher: specs::Dispatcher<'a, 'b>,
                world: specs::World) -> CoreLoop<'a, 'b> {
                CoreLoop {dispatcher, world}
            }
            pub fn display_shit(&self) {
                info!("shit");
            }
            pub fn step(&mut self) {
                
            }
        }

        impl<'a, 'b> game::Loop for CoreLoop<'a, 'b> {
            fn start<F: Fn(Rc<&mut Self>)>(mut self, listener: event::Listener, loop_function: F) {
                let mut time_to_catch_up = std::time::Duration::from_secs(0);
                let update_time = std::time::Duration::from_millis((1.0/60.0 * 1000.0) as u64);
                let mut loop_begin_time = std::time::Instant::now();
                loop {
                    // This dispatches all the systems in parallel (but blocking).
                    // TODO: receive commands
                    
                    let mut duration_of_loop = loop_begin_time.elapsed() + time_to_catch_up;
                    loop_begin_time = std::time::Instant::now();
                    loop {
                        if duration_of_loop >= update_time {
                            loop_function(Rc::new(&mut self));
                
                            if let Some(command) = listener.poll() {
                                debug!("Command received: {:?}", command);
                                match command {
                                    Create => {
                                        
                                    }
                                    _ => {}
                                }
                            }
                            self.dispatcher.dispatch(&mut self.world.res);
                            self.world.maintain();

                            duration_of_loop -= update_time;
                        }
                        else {
                            break;
                        }
                    }
                    time_to_catch_up = update_time - duration_of_loop;
                    let time_for_computations = loop_begin_time.elapsed();
                    if (time_for_computations < update_time) {
                        std::thread::sleep(update_time - time_for_computations);
                    }
                    else {
                        // server has trouble catching up
                        info!("server has trouble (with {} microseconds overtime (update_time: {})", (time_for_computations - update_time).as_micros(), update_time.as_micros());
                    }
                    debug!("time to loop: {} microseconds", loop_begin_time.elapsed().as_micros());
                    debug!("time for computations: {} microseconds", time_for_computations.as_micros());
                }
            }
        }
    }

    #[cfg(feature = "graphical")]
    pub mod core {
        use ::*;
        use std::rc::Rc;
        use nphysics_testbed2d::Testbed; 

        pub struct CoreLoop<'a: 'b, 'b:'static> {
            dispatcher: Option<specs::Dispatcher<'a, 'b>>,
            world: Option<specs::World>,
            testbed: Option<Testbed>
        }

        impl<'a, 'b> CoreLoop<'a, 'b> {
            pub fn new(
                dispatcher: specs::Dispatcher<'a, 'b>,
                world: specs::World,
                physics_world: std::sync::Arc<std::sync::RwLock<nphysics2d::world::World<f32>>>,
                testbed: Testbed) -> CoreLoop<'a, 'b> {
                CoreLoop {dispatcher: Some(dispatcher), world: Some(world), testbed: Some(testbed)}
            }
            /// TODO: fix this duplicated chunk of code (proc macro ? global/static function ?)
            pub fn step(&mut self) {
                    debug!("hey");
                //self.dispatcher.dispatch(&mut self.world.res);
                //self.world.maintain();
            }
        }

    impl<'a, 'b:'static> game::Loop for CoreLoop<'a, 'b> {
        fn start<F: Fn(Rc<&mut Self>)>(mut self, listener: event::Listener,
            loop_function: F) {
            let mut testbed = self.testbed.take().unwrap();
            let ecs_world = RefCell::new(self.world.take().unwrap());
            let dispatcher = RefCell::new(self.dispatcher.take().unwrap());
            debug!("starting");
            let callback = 
            testbed.add_callback(move |w, _, _| {
                if let Some(command) = listener.poll() {
                    debug!("Command received: {:?}", command);
                    match command {
                        Create => {
                            debug!("Creating a box...");
                            ::physics::create_box(&mut ecs_world.borrow_mut(), &mut w.get_mut());
                        }
                        _ => {}
                    }
                }
                // FIXME: make dispatcher work
                let test = ecs_world.borrow_mut().res.has_value::<i32>();
                let test = dispatcher.borrow_mut().dispatch(&mut ecs_world.borrow_mut().res);
                ecs_world.borrow_mut().maintain();
            });

            //testbed.look_at(Point3::new(-4.0, 1.0, -4.0), Point3::new(0.0, 1.0, 0.0));
            testbed.run();
            
        }
    }
    }
}

mod physics {
    use ::*;

    pub fn create_box(ecs_world: &mut World, physics_world: &mut nphysics2d::world::World<f32>) {

        let geom = ShapeHandle::new(Cuboid::new(Vector2::new(
            0.99,
            0.99,
        )));
        let inertia = geom.inertia(1.0);
        let center_of_mass = geom.center_of_mass();
        let x = 2.5;
        let y = 1.1;

        /*
        * Create the rigid body.
        */
        let pos = Isometry2::new(Vector2::new(x, y), 0.0);
        let handle = physics_world.add_rigid_body(pos, inertia, center_of_mass);

        /*
        * Create the collider.
        */
        physics_world.add_collider(
            COLLIDER_MARGIN,
            geom.clone(),
            handle,
            Isometry2::identity(),
            Material::default(),
        );


        // An entity may or may not contain some component.
        // This entity does not have `Vel`, so it won't be dispatched.
        ecs_world.create_entity().with(PhysicsBody(handle)).build();
    }
}

fn main() {
    log4rs::init_file("config/log4rs.yaml", Default::default()).unwrap();

    info!("booting up");
    /*
     * World
     */
    let mut physics_world: nphysics2d::world::World<f32> = nphysics2d::world::World::new();
    physics_world.set_gravity(Vector2::new(0.0, -9.81));

    /*
     * Ground
     */
    let ground_radx = 25.0;
    let ground_rady = 1.0;
    let ground_shape = ShapeHandle::new(Cuboid::new(Vector2::new(
        ground_radx - COLLIDER_MARGIN,
        ground_rady - COLLIDER_MARGIN,
    )));

    let ground_pos = Isometry2::new(-Vector2::y() * ground_rady, na::zero());
    physics_world.add_collider(
        COLLIDER_MARGIN,
        ground_shape,
        BodyHandle::ground(),
        ground_pos,
        Material::default(),
    );
    /*
     * Box
     */
    // The `World` is our
    // container for components
    // and other resources.

    let mut world = World::new();
    world.register::<PhysicsBody>();

    ::physics::create_box(&mut world, &mut physics_world);

    let sharedWorld = Arc::new(RwLock::new(physics_world));

    #[cfg(feature="lightweight")]
    world.add_resource(sharedWorld);
    #[cfg(feature="graphical")]
    world.add_resource(PhysicsWorld::new(Box::new(sharedWorld.clone())));

    #[cfg(feature="graphical")]
    let testbed = nphysics_testbed2d::Testbed::new_with_world_owner(Box::new(sharedWorld.clone()));

    // This builds a dispatcher.
    // The third parameter of `add` specifies
    // logical dependencies on other systems.
    // Since we only have one, we don't depend on anything.
    // See the `full` example for dependencies.
    let dispatcherBuilder = DispatcherBuilder::new();

        #[cfg(feature = "lightweight")]
    let dispatcherBuilder = dispatcherBuilder.with(PhysicsSystem, "physics_system", &[]);
        #[cfg(feature = "graphical")]
    let dispatcherBuilder = dispatcherBuilder.with(PhysicsSystem::new(), "physics_system", &[]);

        //.with(PrinterSystem, "printer_system", &[])
    let dispatcher = dispatcherBuilder.build();

    let (tx, rx) = mpsc::channel();

    let child = std::thread::spawn(move || {
        let inputLoop = input::Loop::new(tx);
        inputLoop.launch();
    });
    let listener = event::Listener::new(rx);
    
    #[cfg(feature = "graphical")]
    let mut looper = game::core::CoreLoop::new(dispatcher, world, sharedWorld.clone(), testbed);
    
    #[cfg(feature = "lightweight")]
    let mut looper = game::core::CoreLoop::new(dispatcher, world);

    use game::Loop;

    looper.start(listener, |mut this| {
        // TODO: I guess a box would be enough here
        let this = &mut *std::rc::Rc::get_mut(&mut this).unwrap();
        this.step();
    });

    
    
    let res = child.join();
}